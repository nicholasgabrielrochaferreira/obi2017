import java.util.Scanner;


public class Chefe {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Scanner st = new Scanner(System.in);
        
        int index1, index2, index3, aux1, aux2; // Auxiliares
        int cSaidas = 0; // Conta o número de linhas que terá na saída
        int cIdades = 0; // Conta o número de superiores que algum funcionário tem
        String instrucao; // Armazena a instrução ("P" ou "T")
        int n = in.nextInt(); // Número de funcionários
        int m = in.nextInt(); // Número de relações de gerência direta
        int i = in.nextInt(); // Número de instruções
        int idades[] = new int[n];// Idades dos superiores de algum funcionário
        
        // Ler idade dos funcionários
        int k[] = new int[n]; // Idade dos funcionários
        for(index1 = 0; index1 < n; index1++){
            k[index1] = in.nextInt();
        }
        
        // Ler relações de gerência direta
        int x[] = new int[m]; // Número do funcionário que gerencia
        int y[] = new int[m]; // Número do funcionário que é gerenciado
        for(index1 = 0; index1 < m; index1++){
            x[index1] = in.nextInt();
            y[index1] = in.nextInt();
        }
        
        // Ler instruções
        String saidas[] = new String[i]; // Armazena as saídas
        for(index1 = 0; index1 < i; index1++){
            instrucao = st.nextLine();
            if(instrucao.equals("T")){
                aux1 = in.nextInt();
                aux2 = in.nextInt();
                // Procurar pelos vetores 'x' e 'y' e trocar os valores inseridos, um pelo outro
                for(index2 = 0; index2 < m; index2++){
                    if(x[index2] == aux1){
                        x[index2] = aux2;
                    } else if(x[index2] == aux2){
                        x[index2] = aux1;
                    }
                    if(y[index2] == aux1){
                        y[index2] = aux2;
                    } else if(y[index2] == aux2){
                        y[index2] = aux1;
                    }
                }
            } else { // Instrução = "P"
                cIdades = 0;
                aux1 = in.nextInt();
                // Armazenar as idades de seus superiores em um vetor
                for(index2 = 0; index2 < n; index2++){
                    if(y[index2] == aux1){ // , então x[index2] é um superior de aux1
                        // x[index2] é o número do funcionário superior à aux1
                        idades[cIdades] = k[index2]; // Armazenar a idade do funcionário x[index2]
                        cIdades++;
                    }
                }
                // Se não encontrar nenhum superior, definir essa saída "*"
                if(cIdades == 0){
                    saidas[cSaidas] = "*";
                } else { // Ordenar o vetor das idades dos superiores em ordem crescente
                    for(index2 = 0; index2 < n; index2++){
                        for(index3 = 0; index3 < n - 1; index3++){
                            if(idades[index3] > idades[index3 + 1]){
                                aux1 = idades[index3];
                                idades[index3] = idades[index3 + 1];
                                idades[index3 + 1] = aux1;
                            }
                        }
                    }
                    // Essa saída será a menor dentre as idades dos superiores do funcionário aux1
                    saidas[cSaidas] = String.valueOf(idades[cIdades+1]);
                }
                cSaidas++;
            }
        }
        // Imprimir saída
        System.out.println("------------------");
        for(index1 = 0; index1 < cSaidas; index1++){
            System.out.println(saidas[index1]);
        }
    }
}
