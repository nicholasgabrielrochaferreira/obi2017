
import java.util.Scanner;

public class BotasTrocadas {

    public static void main(String[] args) {
        // Inicialização e leitura de variáveis
        Scanner in = new Scanner(System.in);
        Scanner in2 = new Scanner(System.in);

        int aux, x = 0; 
        String aux2;
        int n = in.nextInt();
        int m[] = new int[n];
        String l[] = new String[n];

        for (int i = 0; i < n; i++) {
            m[i] = in.nextInt();
            l[i] = in.nextLine();
        }

        // Sortear de acordo com tamanho do sapato
        for (int i = 0; i < n; i++) {
            for (int i2 = 0; i2 < n - 1; i2++) {
                if (m[i2] > m[i2 + 1]) {
                    aux = m[i2];
                    m[i2] = m[i2 + 1];
                    m[i2 + 1] = aux;
                    
                    aux2 = l[i2];
                    l[i2] = l[i2 + 1];
                    l[i2 + 1] = aux2;
                }
            }
        }
        // Verificar pares
        for(int i = 0; i < n - 1; i++){
            if(m[i] == m[i+1] && !l[i].equals(l[i+1])){
                x++;
                i++;
            }
        }
        System.out.println(x);
    }
}
