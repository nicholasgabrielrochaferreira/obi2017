
import java.util.Scanner;


public class Game10 {
    public static void main(String[] args) {
        int n, d, a, x;
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        d = in.nextInt();
        a = in.nextInt();
        
        x = d - a;
        
        if(x < 0){
            x += n;
        }
        
        System.out.println(x);
    }
}
